using System;
using System.ComponentModel.DataAnnotations; 
using Microsoft.AspNetCore.Identity; 


namespace Drugdealer.Model
{
 public class NewsUser :  IdentityUser{   
         public string  FirstName { get; set;}      
         public string  LastName { get; set;}   
           

  } 
 
  
  public class Drug
  {    public int  DrugID { get; set;}        
       public string NewsUserId {get; set;}   
       public string Name {get; set;}
       public string Category {get; set;} 
       public int  Number {get; set;}
       public NewsUser  postUser {get; set;} 
    } 

}