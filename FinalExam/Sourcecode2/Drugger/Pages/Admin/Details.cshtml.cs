using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Drugdealer.Data;
using Drugdealer.Model;

namespace Drugger.Pages.Admin
{
    public class DetailsModel : PageModel
    {
        private readonly Drugdealer.Data.DrugReportContext _context;

        public DetailsModel(Drugdealer.Data.DrugReportContext context)
        {
            _context = context;
        }

        public Drug Drug { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Drug = await _context.drugList
                .Include(d => d.postUser).FirstOrDefaultAsync(m => m.DrugID == id);

            if (Drug == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
