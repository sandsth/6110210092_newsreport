using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Drugdealer.Data;
using Drugdealer.Model;

namespace Drugger.Pages.Admin
{
    public class IndexModel : PageModel
    {
        private readonly Drugdealer.Data.DrugReportContext _context;

        public IndexModel(Drugdealer.Data.DrugReportContext context)
        {
            _context = context;
        }

        public IList<Drug> Drug { get;set; }

        public async Task OnGetAsync()
        {
            Drug = await _context.drugList
                .Include(d => d.postUser).ToListAsync();
        }
    }
}
