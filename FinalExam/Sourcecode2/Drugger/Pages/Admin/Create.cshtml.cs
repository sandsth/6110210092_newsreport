using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Drugdealer.Data;
using Drugdealer.Model;

namespace Drugger.Pages.Admin
{
    public class CreateModel : PageModel
    {
        private readonly Drugdealer.Data.DrugReportContext _context;

        public CreateModel(Drugdealer.Data.DrugReportContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["NewsUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Drug Drug { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.drugList.Add(Drug);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}