﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Drugdealer.Model;
using Drugdealer.Data;
namespace Drugger.Pages
{
    public class IndexModel : PageModel
    {
        private readonly Drugdealer.Data.DrugReportContext _context;
    public IndexModel(Drugdealer.Data.DrugReportContext context)
 {
        _context = context;
 }

 public IList<Drug> Drugs { get;set; }
 public async Task OnGetAsync()
 {
    Drugs = await _context.drugList.ToListAsync();
 } 
    }
}
