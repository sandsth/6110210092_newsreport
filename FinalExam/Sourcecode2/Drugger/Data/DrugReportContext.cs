using Microsoft.EntityFrameworkCore;
using Drugdealer.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;


namespace Drugdealer.Data
{
 public class DrugReportContext :  IdentityDbContext<NewsUser> 
 {
     
    public DbSet<Drug> drugList { get; set; }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
 {
        optionsBuilder.UseSqlite(@"Data source=drugReport.db");
 }
 }
}
